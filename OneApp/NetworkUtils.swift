//
//  NetworkUtils.swift
//  IP Menu
//
//  Created by Guy Pascarella on 9/16/15.
//
//

import Foundation

class NetworkUtils {

    // http://stackoverflow.com/questions/28084853/how-to-get-the-local-host-ip-address-on-iphone-in-swift
    static func getIFAddresses() -> [String: [sa_family_t: [String]]] {
        var addresses = [String: [sa_family_t: [String]]]();

        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {

            // For each interface ...
            var ptr = ifaddr;
            while ( ptr != nil ) {
            //for (var ptr = ifaddr; ptr != nil; ptr = ptr?.pointee.ifa_next) {
                let flags = Int32((ptr?.pointee.ifa_flags)!)
                var addr = ptr?.pointee.ifa_addr.pointee
                let name = String(cString: (ptr?.pointee.ifa_name)!);

                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr?.sa_family == UInt8(AF_INET) || addr?.sa_family == UInt8(AF_INET6) {

                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        if (getnameinfo(&addr!, socklen_t((addr?.sa_len)!), &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                                guard let address = String(validatingUTF8: hostname) else {
                                    continue;
                                }

                                // Note: If there are multiple addresses (eg ipv4 and ipv6)
                                // associated with the same interface then whatever the last
                                // one in is wins.
                                ConsoleLog.debug("Saving \(name) with \(address) and flags \(flags)");

                                if ( nil == addresses[name] ) {
                                    addresses[name] = [sa_family_t:[String]]();
                                }

                                if ( nil == addresses[name]![(addr?.sa_family)!] ) {
                                    addresses[name]![(addr?.sa_family)!] = [String]();
                                }
                                addresses[name]![(addr?.sa_family)!]?.append(address);
                        }
                    }
                }
                ptr = ptr?.pointee.ifa_next
            }
            freeifaddrs(ifaddr)
        }

        return addresses
    }

    // Retrieve the default interface name that requests are routed through
    // using shell commands, which are more stable than converted code
    // For example, "en0" or "utun0"
    static func getDefaultGatewayInterfaceShell() -> String? {
        // http://practicalswift.com/2014/06/25/how-to-execute-shell-commands-from-swift/
        let task = Process()
        task.launchPath = "/sbin/route"
        task.arguments = ["get", "0.0.0.0"]
        
        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)

        // Scan output for "^\s*interface:\s+(ifX)\s*$"
        let lines = output?.components(separatedBy: "\n");
        for line in lines! {
            //let line2 = line.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            if let range = line.range(of: "interface: ") {
              //return line.substring(from: range.upperBound);
            return String(line[range.upperBound...]);
            }
        }

        return nil;
    }
}
