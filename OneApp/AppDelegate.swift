//
//  AppDelegate.swift
//  OneApp
//
//  Created by Kang Risselada on 12/03/2017.
//  Copyright © 2017 Swift. All rights reserved.
//

import Cocoa
//  import AppleScriptKit
import Foundation


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    

    @IBOutlet weak var statusMenu: NSMenu!
    @IBOutlet weak var hFiles: NSButtonCell!
    @IBOutlet weak var infoWindow: NSWindow!


    
    
    let statusItem = NSStatusBar.system.statusItem(withLength: -1)
    
    internal var timer: Timer?;
    internal var addresses: [String:[sa_family_t:[String]]]?;
    internal var defaultIF: String?;
    
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let icon = NSImage(named: "statusIcon")
        icon?.isTemplate = true
        
        statusItem.image = icon
        statusItem.menu = statusMenu
        
        updateIPAddress();
        
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(AppDelegate.updateIPAddress), userInfo: nil, repeats: true);
        timer!.tolerance = 10;
        
        let currentHost = Host.current().localizedName ?? ""
        ComputerName.stringValue = currentHost
        
    }

        func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    //------------------------------------------------------------------------------------------------
    //
    //  System
    //
    //  Computer Name
    //
    @IBOutlet weak var ComputerName: NSTextFieldCell!
  
    
    //------------------------------------------------------------------------------------------------
    //
    //  System
    //
    //  Public IP Address
    //
    @IBOutlet weak var publicIP: NSTextFieldCell!
    
    
    //------------------------------------------------------------------------------------------------
    //
    //  System
    //
    //  IP Addresses - Internal & Public
    //
    
    @IBOutlet weak var IPaddr: NSTextFieldCell!
    
    @objc func updateIPAddress() {
        let _addresses = NetworkUtils.getIFAddresses();
        // Disable this for now because it looks like it may be causing the OS to deadlock
        //var _defaultIF: String? = "en0"; //NetworkUtils.getDefaultGatewayInterface();
        var _defaultIF: String? = NetworkUtils.getDefaultGatewayInterfaceShell();
        
        let equal = compareAddresses(self.addresses, newA: _addresses);
        
        if ( !equal ) {
            ConsoleLog.debug("Detected new addresses \(String(describing: addresses)) -> \(_addresses)");
            
            addresses = _addresses;
            
        }
        
        if ( nil == _defaultIF ) {
            _defaultIF = "en0";
        }
        
        // Debug
        if ( nil == defaultIF || ComparisonResult.orderedSame != _defaultIF!.compare(defaultIF!) ) {
            ConsoleLog.debug("Detected new default interface (\(String(describing: defaultIF)) -> \(String(describing: _defaultIF)))");
        }
        
        // Pick the default address as the title
        
        var addr = "127.0.0.1"
        if ( nil == defaultIF || ComparisonResult.orderedSame != _defaultIF!.compare(defaultIF!) || !equal ) {
            defaultIF = _defaultIF;
            
            if ( nil != addresses && nil != addresses![defaultIF!] ) {
                // Prefer ipv4 over ipv6
                let defaultProtoMap = addresses![defaultIF!];
                let ipv4 = defaultProtoMap![UInt8(AF_INET)];
                let ipv6 = defaultProtoMap![UInt8(AF_INET6)];
                if ( nil != ipv4 && ipv4!.count > 0 ) {
                    addr = ipv4![0];
                } else if ( nil != ipv6 && ipv6!.count > 0 ) {
                    addr = ipv6![0];
                } else {
                    print("No ipv4 or ipv6 addresses detected");
                    addr = "127.0.0.1";
                }
            }
            
            print(addr)
            IPaddr.stringValue = addr
        }
        else {
            print("No Changes \(String(describing: defaultIF)), \(String(describing: _defaultIF)), \(equal)");
        }

        if let url = URL(string: "https://api.ipify.org") {
            do {
                let contents = try String(contentsOf: url)
                print(contents)
                publicIP.stringValue = contents
            } catch {
                // contents could not be loaded
            }
        } else {
            // the URL was bad!
        }
        
    }
    
    func compareAddresses(_ oldA:[String:[sa_family_t:[String]]]?, newA:[String:[sa_family_t:[String]]]) -> Bool {
        if ( nil == oldA || newA.count != oldA!.count ) {
            return false;
        } else {
            // count is equal, but contents may be different, so
            // iterate over the new addresses
            for (name,newProtoMap) in newA {
                
                // Check to see if this interface is previously seen
                guard let oldProtoMap = oldA![name] else {
                    return false;
                }
                
                for (newProto,newAddresses) in newProtoMap {
                    guard let oldAddresses = oldProtoMap[newProto] else {
                        return false;
                    }
                    
                    // Check the actual addresses
                    var found = false;
                    for newAddr in newAddresses {
                        for oldAddr in oldAddresses {
                            // Now check if there are the same addresses as previous
                            if ( ComparisonResult.orderedSame == newAddr.compare(oldAddr) ) {
                                found = true;
                                break;
                            }
                        }
                        if ( !found ) {
                            return false;
                        }
                    }
                }
            }
        }
        
        return true;
    }

    //------------------------------------------------------------------------------------------------
    //
    //  Rebuild Network Shares
    //
    @IBAction func rebuildNShares(_ sender: Any) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["/Users/Shared/loginClient/Shares.scpt"]
        
        task.launch()
    
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
    

        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
    
        escd?.flags = CGEventFlags.maskCommand;
    
        let loc = CGEventTapLocation.cghidEventTap
    
        escd?.post(tap: loc)
        escu?.post(tap: loc)

    }
    
    //------------------------------------------------------------------------------------------------
    //
    //  Rebuild Printerlist
    //
    @IBAction func rebuildPrinterlist(_ sender: Any) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["/Users/Shared/loginClient/Rebuild_Printer_List.scpt"]
        
        task.launch()
        
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
        escd?.flags = CGEventFlags.maskCommand;
        
        let loc = CGEventTapLocation.cghidEventTap
        
        escd?.post(tap: loc)
        escu?.post(tap: loc)
    
    }
    
    //------------------------------------------------------------------------------------------------
    //
    //  Reset Printing System
    //
    @IBAction func resetPrintingSystem(_ sender: Any) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["/Users/Shared/loginClient/Reset_Printing_System.scpt"]
        
        task.launch()
        
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
        escd?.flags = CGEventFlags.maskCommand;
        
        let loc = CGEventTapLocation.cghidEventTap
        
        escd?.post(tap: loc)
        escu?.post(tap: loc)
    
    }
  
    //------------------------------------------------------------------------------------------------
    //
    //  Update loginClient folder
    //
    @IBAction func loginClient(_ sender: NSButton) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["/Users/Shared/loginClient/loginUpdates.scpt"]
        
        task.launch()
        
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
        escd?.flags = CGEventFlags.maskCommand;
        
        let loc = CGEventTapLocation.cghidEventTap
        
        escd?.post(tap: loc)
        escu?.post(tap: loc)
    }
    
    //------------------------------------------------------------------------------------------------
    //
    //  Toggle Hidden Files
    //
    @IBAction func hiddenFiles(_ sender: NSButton) {
        let task = Process()
        task.launchPath = "/usr/bin/defaults"
        if(hFiles.state == .off)
        {
            task.arguments = ["write", "com.apple.finder", "AppleShowAllFiles", "YES"]
            hFiles.state = .on
        }
    
       else {
            hFiles.state = .on
            task.arguments = ["write", "com.apple.finder", "AppleShowAllFiles", "NO"]
            hFiles.state = .on
        }
            task.launch()
            task.waitUntilExit()
        
        let killtask = Process()
        killtask.launchPath = "/usr/bin/killall"
        killtask.arguments = ["Finder"]
        killtask.launch()
    }


    //------------------------------------------------------------------------------------------------
    //
    //  Force Shutdown
    //
    @IBAction func fShutdown(_ sender: NSButton) {
        let task = Process()
        task.launchPath = "/sbin/shutdown"
        task.arguments = [" -h +0"]
        task.launch()
        task.waitUntilExit()
    
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
        escd?.flags = CGEventFlags.maskCommand;
        
        let loc = CGEventTapLocation.cghidEventTap
        
        escd?.post(tap: loc)
        escu?.post(tap: loc)

    }

    
    
    
    //------------------------------------------------------------------------------------------------
    //
    //  Force Restart
    //
    @IBAction func fRestart(_ sender: NSButton) {
        let task = Process()
        task.launchPath = "/sbin/shutdown"
        task.arguments = [" -r +0"]
        task.launch()
        task.waitUntilExit()
    
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
        escd?.flags = CGEventFlags.maskCommand;
        
        let loc = CGEventTapLocation.cghidEventTap
        
        escd?.post(tap: loc)
        escu?.post(tap: loc)

    }
 
    //------------------------------------------------------------------------------------------------
    //
    //  Lock Screen
    //
    @IBAction func Lock(_ sender: NSButton) {
        let libHandle = dlopen("/System/Library/PrivateFrameworks/login.framework/Versions/Current/login", RTLD_LAZY)
        let sym = dlsym(libHandle, "SACLockScreenImmediate")
        typealias myFunction = @convention(c) () -> Void
        let SACLockScreenImmediate = unsafeBitCast(sym, to: myFunction.self)
        SACLockScreenImmediate()
    
    }

    //------------------------------------------------------------------------------------------------
    //
    //  Quit menu button routine
    //
      @IBAction func quitMenu(_ sender: NSButton) {
        
        let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
        let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
        let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
        escd?.flags = CGEventFlags.maskCommand;
        
        let loc = CGEventTapLocation.cghidEventTap
        
        escd?.post(tap: loc)
        escu?.post(tap: loc)
    
      }
    
    //------------------------------------------------------------------------------------------------
    //
    //  Quit menu button routine
    //
    //  @IBAction func quitMenu(_ sender: NSButton) {
        
    //    let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
    //    let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
    //    let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
    //    escd?.flags = CGEventFlags.maskCommand;
        
    //    let loc = CGEventTapLocation.cghidEventTap
        
    //    escd?.post(tap: loc)
    //    escu?.post(tap: loc)
        
    //  }

    //------------------------------------------------------------------------------------------------
    //
    //  Quit menu button routine
    //
    //  @IBAction func quitMenu(_ sender: NSButton) {
        
    //    let src = CGEventSource(stateID: CGEventSourceStateID.hidSystemState)
        
        
    //    let escd = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: true)
    //    let escu = CGEvent(keyboardEventSource: src, virtualKey: 0x35, keyDown: false)
        
    //    escd?.flags = CGEventFlags.maskCommand;
        
    //    let loc = CGEventTapLocation.cghidEventTap
        
    //    escd?.post(tap: loc)
    //    escu?.post(tap: loc)
        
    //  }
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    


